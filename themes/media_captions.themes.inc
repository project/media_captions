<?php

/**
 * @file
 * Theme preprocess functions for the Media: Captions module.
 */

function template_preprocess_media_captions_ttml(&$variables) {
  $variables = array_merge($variables, $variables['variables']);

  $variables['frames'] = array();
  foreach ($variables['media_captions']['frames'] as $frame) {
    $variables['frame'] = $frame;
    $variables['frames'][] = theme('media_captions_ttml_frame', $variables);
  }
  $variables['captions'] = implode("\n      ", $variables['frames']);
}

function template_preprocess_media_captions_ttml_frame(&$variables) {
  $variables = array_merge($variables, $variables['variables']);

  $variables['begin'] = theme('media_captions_time', $variables['frame']['times'][0]);
  $variables['end'] = theme('media_captions_time', $variables['frame']['times'][1]);
  $variables['text'] = str_replace("\n", '<br />', check_plain(implode("\n", $variables['frame']['text'])));
}

function theme_media_captions_time($timestamp) {
  list($time) = explode('.', $timestamp);
  return check_plain($time);
}
