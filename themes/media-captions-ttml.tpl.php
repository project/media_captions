<?php

/**
 * @file
 * Template file for theme('media_captions_ttml').
 */
?>
<tt xml:lang="en">
  <head>
    <layout/>
  </head>
  <body>
    <div xml:id="captions">
      <?php print $captions; ?>

    </div>
  </body>
</tt>
