<?php

/**
 * @file
 * Implementations of emfield hooks for the Media: Captions module.
 */

/**
 * Implements hook_emfield_widget_extra().
 *
 * This is called by _emfield_emfield_widget (in emfield.cck.inc) when
 * building the widget on the node form. It creates a textarea element
 * so the editor can upload captions.
 */
function _media_captions_emfield_widget_extra($form, $form_state, $field, $items, $delta = 0, $module) {
  $element = array();
  if ($field['widget']['media_captions']) {
    $label = $field['widget']['media_captions_label'];
    $description = $field['widget']['media_captions_description'];
    $default_value = isset($items[$delta]['data']['media_captions']) ? $items[$delta]['data']['media_captions']['raw'] : '';
    $element['media_captions'] = array(
      '#type' => 'textarea',
      '#title' => t('@label', array('@label' => $label)),
      '#description' => filter_xss($description),
      '#default_value' => $default_value,
    );
  }
  return $element;
}

/**
 *  This provides extra widget settings to emfields.
 *  A checkbox to allow captions.
 */
function _media_captions_emfield_widget_settings_extra($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['media_captions'] = array(
        '#type' => 'fieldset',
        '#title' => t('Captions'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['media_captions']['media_captions'] = array(
        '#type' => 'checkbox',
        '#title' => t('Store captions for this field'),
        '#description' => t('If checked, then editors may upload captions for this media.'),
        '#default_value' => isset($widget['media_captions']) ? $widget['media_captions'] : media_captions_variable_get('default_captions'),
      );
      $form['media_captions']['media_captions_label'] = array(
        '#type' => 'textfield',
        '#title' => t('Captions label'),
        '#default_value' => isset($widget['media_captions_label']) ? $widget['media_captions_label'] : media_captions_variable_get('label'),
        '#description' => t('This label will be displayed when uploading captions.'),
      );
      $form['media_captions']['media_captions_description'] = array(
        '#type' => 'textarea',
        '#title' => t('Captions description'),
        '#default_value' => isset($widget['media_captions_description']) ? $widget['media_captions_description'] : media_captions_variable_get('description'),
        '#description' => t('This description will be displayed when uploading captions.'),
      );
      return $form;
    case 'save':
      return array('media_captions', 'media_captions_label', 'media_captions_description');
  }
}

/**
 *  Implements hook_emfield_field_extra().
 *  This is called on field operations to allow us to add captions.
 */
function _media_captions_emfield_field_extra($op, &$node, $field, &$items, $teaser, $page, $module) {
  switch ($op) {
    case 'insert':
    case 'update':
      // Called before content.module defaults.
      foreach ($items as $delta => $item) {
        $items[$delta]['data']['media_captions'] = array('raw' => $items[$delta]['media_captions']['media_captions'], 'frames' => media_captions_parse_data($items[$delta]['media_captions']['media_captions']));
      }
      break;
  }
}
