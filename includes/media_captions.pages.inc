<?php

/**
 * @file
 * Page callbacks for the Media: Captions module.
 */

/**
 * Page callback for media-captions/emfield/%node/cc.ttml.
 */
function media_captions_emfield_ttml($node) {
  $variables = array(
    'node' => $node,
    'field_name' => $_GET['field_name'],
    'delta' => $_GET['delta'],
  );
  $variables['field'] = content_fields($variables['field_name']);

  // Respect any field access permissions.
  if (!content_access('view', $variables['field'], NULL, $variables['node'])) {
    drupal_access_denied();
    return;
  }

  // Suppress the Admin Menu, if that module/fuction exists.
  if (function_exists('admin_menu_suppress')) {
    admin_menu_suppress();
  }

  $variables['item'] = $node->{$variables['field_name']}[$variables['delta']];
  $variables['media_captions'] = $variables['item']['data']['media_captions'];

  drupal_set_header('Content-Type: application/ttml+xml; charset=utf-8');

  print theme('media_captions_ttml', $variables);
}
