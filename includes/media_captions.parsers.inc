<?php

/**
 * @file
 * Parsers for the Media: Captions module.
 */

/**
 * Parses the data from a .sbv format file into a captions frames array.
 */
function _media_captions_parse_sbv_data($data) {
  $frames = array();
  $frame_count = 0;
  $new_line = $catch_time = TRUE;

  // Read a line at a time.
  foreach (explode("\n", $data) as $line) {
    $line = trim($line);

    // The .sbv format automatically increments the frame counter.
    if ($new_line) {
      $frame_count++;
      $new_line = FALSE;
    }

    if ($catch_time) {
      $times = _media_captions_times($line, ',');
      if ($times) {
        $frames[$frame_count]['times'] = $times;
        $catch_time = FALSE;
      }
    }
    else if ($line) {
      $frames[$frame_count]['text'][] = $line;
    }
    else {
      $new_line = $catch_time = TRUE;
    }
  }
  return $frames;
}

/**
 * Parses the data from a .srt format file into a captions frames array.
 */
function _media_captions_parse_srt_data($data) {
  $frames = array();
  $new_line = $catch_time = TRUE;

  // Read a line at a time.
  foreach (explode("\n", $data) as $line) {
    $line = trim($line);
    if ($new_line) {
      $frame_count = intval($line);
      if ($frame_count) {
        $new_line = FALSE;
      }
    }
    else if ($catch_time) {
      $times = _media_captions_times($line, ' --> ');
      if ($times) {
        $frames[$frame_count]['times'] = $times;
        $catch_time = FALSE;
      }
    }
    else if ($line) {
      $frames[$frame_count]['text'][] = $line;
    }
    else {
      $new_line = $catch_time = TRUE;
    }
  }
  return $frames;
}

/**
 * Ensures the line is composed of two valid timestamps, like HH:MM:SS.000.
 */
function _media_captions_times($line, $separator) {
  // See if we have two timestamps.
  $times = explode($separator, $line);
  if (count($times) != 2) {
    return FALSE;
  }

  // Check for valid times.
  foreach ($times as $key => $time) {
    $time = str_replace(',', '.', $time);

    // Convert the timestamp for error checking.
    $date = date_parse($time);

    // Return FALSE if this item is not a timestamp.
    if ($date['error_count']) {
      return FALSE;
    }

    $times[$key] = sprintf("%02d:%02d:%02d.%03d", $date['hour'], $date['minute'], $date['second'], $date['fraction']);
  }

  // The times are ok, so return them.
  return $times;
}
